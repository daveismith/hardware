<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.1.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="LETTER_L">
<frame x1="0" y1="0" x2="248.92" y2="185.42" columns="12" rows="17" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94" font="vector">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LETTER_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
LETTER landscape</description>
<gates>
<gate name="G$1" symbol="LETTER_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="147.32" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="WS2812B">
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-1.6" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.6" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-1.6" y2="2.5" width="0.127" layer="21"/>
<smd name="1-VDD" x="2.45" y="-1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="2-DOUT" x="2.45" y="1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="4-DIN" x="-2.45" y="-1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="3-GND" x="-2.45" y="1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<circle x="0" y="0" radius="1.7204625" width="0.127" layer="21"/>
<text x="3.4925" y="1.5875" size="0.8128" layer="25" ratio="10" rot="R270">&gt;NAME</text>
<wire x1="-1.6" y1="2.5" x2="-1.3" y2="2.8" width="0.127" layer="21"/>
<wire x1="-1.3" y1="2.8" x2="-1.7" y2="3.2" width="0.127" layer="21"/>
<wire x1="-1.7" y1="3.2" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="1X03">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" rot="R90"/>
<text x="-3.8862" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="1X03-CLEANBIG">
<pad name="1" x="-2.54" y="0" drill="1.016" diameter="1.6764" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.6764" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.6764" rot="R90"/>
<text x="-3.8862" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="WS2812B">
<pin name="DIN" x="-5.08" y="0" visible="pad" length="short" direction="in"/>
<pin name="DOUT" x="5.08" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<pin name="VDD" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="GND" x="0" y="-5.08" visible="pad" length="short" rot="R90"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.032" y1="0.254" x2="-1.778" y2="0" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0" x2="-2.032" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.778" y1="0.254" x2="2.032" y2="0" width="0.254" layer="94"/>
<wire x1="2.032" y1="0" x2="1.778" y2="-0.254" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="-0.762" y2="0.508" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0.508" x2="0" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0" y1="-0.762" x2="0.762" y2="0.508" width="0.254" layer="94"/>
<wire x1="0.762" y1="0.508" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="-0.762" x2="0" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-0.762" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="-1.27" x2="1.016" y2="-1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="3.048" size="0.6096" layer="95">&gt;NAME</text>
</symbol>
<symbol name="PINHD3">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="WS2812B" prefix="L">
<gates>
<gate name="G$1" symbol="WS2812B" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WS2812B">
<connects>
<connect gate="G$1" pin="DIN" pad="4-DIN"/>
<connect gate="G$1" pin="DOUT" pad="2-DOUT"/>
<connect gate="G$1" pin="GND" pad="3-GND"/>
<connect gate="G$1" pin="VDD" pad="1-VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X3" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X03">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CB" package="1X03-CLEANBIG">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="5V">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="5V" prefix="SUPPLY">
<gates>
<gate name="G$1" symbol="5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MMB_Common_rev82">
<description>&lt;b&gt;PCB Libraries Packages&lt;/b&gt;&lt;p&gt;
\</description>
<packages>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.9435" y1="0.4145" x2="0.9435" y2="0.4145" width="0.1524" layer="51"/>
<wire x1="0.9435" y1="-0.4145" x2="-0.9435" y2="-0.4145" width="0.1524" layer="51"/>
<wire x1="-0.965" y1="0.483" x2="0.965" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0.965" y1="0.483" x2="0.965" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="0.965" y1="-0.483" x2="-0.965" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="-0.483" x2="-0.965" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0.9562" y1="-0.4018" x2="0.9615" y2="0.411" width="0.1524" layer="51"/>
<wire x1="-0.9488" y1="0.411" x2="-0.9435" y2="-0.4145" width="0.1524" layer="51"/>
<smd name="1" x="-0.4722" y="0" dx="0.6604" dy="0.6604" layer="1"/>
<smd name="2" x="0.4722" y="0" dx="0.6604" dy="0.6604" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VAL</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.397" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VAL</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C-EU" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="0.1UF">
<attribute name="DESCRIPTION" value="CAPACITOR, .1UF 10V 10% X5R 0402" constant="no"/>
<attribute name="MFR" value="Murata" constant="no"/>
<attribute name="MFR_PN" value="GRM155R61A104KA01D" constant="no"/>
<attribute name="VAL" value="0.1uF" constant="no"/>
</technology>
<technology name="0.47UF">
<attribute name="DESCRIPTION" value="CAPACITOR, 0.47UF, +/-10%, 10V, CERAMIC, X5R, 0402" constant="no"/>
<attribute name="MFR" value="Murata" constant="no"/>
<attribute name="MFR_PN" value="GRM155R61A474KE15D" constant="no"/>
<attribute name="VAL" value="0.47uF" constant="no"/>
</technology>
<technology name="100NF">
<attribute name="DESCRIPTION" value="CAPACITOR, 100NF, +/-10%, 16V, X7R, 0402" constant="no"/>
<attribute name="MFR" value="Murata" constant="no"/>
<attribute name="MFR_PN" value="GRM155R71C104KA88D" constant="no"/>
<attribute name="VAL" value="100nF" constant="no"/>
</technology>
<technology name="10NF">
<attribute name="DESCRIPTION" value="CAPACITOR, 10NF, +/-10%, 25V, X7R, 0402" constant="no"/>
<attribute name="MFR" value="Murata" constant="no"/>
<attribute name="MFR_PN" value="GRM155R71E103KA01D" constant="no"/>
<attribute name="VAL" value="10nF" constant="no"/>
</technology>
<technology name="18PF">
<attribute name="DESCRIPTION" value="CAPACITOR, 18PF, +/-5%, 50V, C0G, NPO, 0402" constant="no"/>
<attribute name="MFR" value="Murata" constant="no"/>
<attribute name="MFR_PN" value="GRM1555C1H180JZ01D" constant="no"/>
<attribute name="VAL" value="18pF" constant="no"/>
</technology>
<technology name="1UF">
<attribute name="DESCRIPTION" value="CAPACITOR, 1.0UF, +/-10%, 6.3V, X5R, 0402" constant="no"/>
<attribute name="MFR" value="Murata" constant="no"/>
<attribute name="MFR_PN" value="GRM155R60J105KE19D" constant="no"/>
<attribute name="VAL" value="1uF" constant="no"/>
</technology>
<technology name="27PF">
<attribute name="DESCRIPTION" value="CAPACITOR, 27PF, +/-5%, 50V, C0G, NPO, 0402" constant="no"/>
<attribute name="MFR" value="Murata" constant="no"/>
<attribute name="MFR_PN" value="GRM1555C1H270JZ01D" constant="no"/>
<attribute name="VAL" value="27pF" constant="no"/>
</technology>
<technology name="8PF">
<attribute name="DESCRIPTION" value="CAPACITOR, 8.0PF, 0.5PF, 50V, C0G, NPO, 0402" constant="no"/>
<attribute name="MFR" value="Murata" constant="no"/>
<attribute name="MFR_PN" value="GRM1555C1H8R0DZ01D" constant="no"/>
<attribute name="VAL" value="8pF" constant="no"/>
</technology>
<technology name="VACANT">
<attribute name="DESCRIPTION" value="Vacant C0402" constant="no"/>
<attribute name="MFR" value="" constant="no"/>
<attribute name="MFR_PN" value="" constant="no"/>
<attribute name="VAL" value="Vacant" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="MFR" value="" constant="no"/>
<attribute name="MFR_PN" value="" constant="no"/>
<attribute name="VAL" value="" constant="no"/>
</technology>
<technology name="2.2UF">
<attribute name="DESCRIPTION" value="CAPACITOR, 2.2UF, +/-10%, 16V, CERAMIC, X5R, 0603" constant="no"/>
<attribute name="MFR" value="Murata" constant="no"/>
<attribute name="MFR_PN" value="GRM188R61C225KE15D" constant="no"/>
<attribute name="VAL" value="2.2uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" deviceset="LETTER_L" device="" value="test"/>
<part name="L1" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L2" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L3" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L4" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L5" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L6" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L7" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L8" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L9" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L10" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L11" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L12" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L13" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L14" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L15" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L16" library="adafruit" deviceset="WS2812B" device=""/>
<part name="SUPPLY1" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY3" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY4" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY5" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY6" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY7" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY8" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY9" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY10" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY11" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY12" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY13" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY14" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY15" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY16" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY17" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY18" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY19" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY20" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY21" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY22" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY23" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY24" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY25" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY26" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY27" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY28" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY29" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY30" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY31" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY32" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="C1" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C2" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C3" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C4" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C5" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C6" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C7" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C8" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C9" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C10" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C11" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C12" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C13" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C14" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C15" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C16" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="JP1" library="adafruit" deviceset="PINHD-1X3" device=""/>
<part name="JP2" library="adafruit" deviceset="PINHD-1X3" device=""/>
<part name="SUPPLY33" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY34" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY35" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY36" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="L17" library="adafruit" deviceset="WS2812B" device=""/>
<part name="SUPPLY37" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY38" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="C17" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="L18" library="adafruit" deviceset="WS2812B" device=""/>
<part name="SUPPLY39" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY40" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="C18" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="L19" library="adafruit" deviceset="WS2812B" device=""/>
<part name="SUPPLY41" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY42" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="C19" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="L20" library="adafruit" deviceset="WS2812B" device=""/>
<part name="SUPPLY43" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY44" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="C20" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="L21" library="adafruit" deviceset="WS2812B" device=""/>
<part name="SUPPLY45" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY46" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="C21" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="L22" library="adafruit" deviceset="WS2812B" device=""/>
<part name="SUPPLY47" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY48" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="C22" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="L23" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L24" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L25" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L26" library="adafruit" deviceset="WS2812B" device=""/>
<part name="L27" library="adafruit" deviceset="WS2812B" device=""/>
<part name="SUPPLY49" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY50" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY51" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY52" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY53" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY54" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY55" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY56" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY57" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="SUPPLY58" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="C23" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C24" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C25" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C26" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="C27" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="L28" library="adafruit" deviceset="WS2812B" device=""/>
<part name="SUPPLY59" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY60" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="C28" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="L29" library="adafruit" deviceset="WS2812B" device=""/>
<part name="SUPPLY61" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY62" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="C29" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="L30" library="adafruit" deviceset="WS2812B" device=""/>
<part name="SUPPLY63" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY64" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="C30" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
<part name="L31" library="adafruit" deviceset="WS2812B" device=""/>
<part name="SUPPLY65" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY66" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="C31" library="MMB_Common_rev82" deviceset="C-EU" device="C0603" value="0.1uF"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="FRAME1" gate="G$2" x="147.32" y="0"/>
<instance part="L1" gate="G$1" x="20.32" y="152.4"/>
<instance part="L2" gate="G$1" x="40.64" y="152.4"/>
<instance part="L3" gate="G$1" x="60.96" y="152.4"/>
<instance part="L4" gate="G$1" x="81.28" y="152.4"/>
<instance part="L5" gate="G$1" x="101.6" y="152.4"/>
<instance part="L6" gate="G$1" x="121.92" y="152.4"/>
<instance part="L7" gate="G$1" x="142.24" y="152.4"/>
<instance part="L8" gate="G$1" x="162.56" y="152.4"/>
<instance part="L9" gate="G$1" x="182.88" y="152.4"/>
<instance part="L10" gate="G$1" x="203.2" y="152.4"/>
<instance part="L11" gate="G$1" x="223.52" y="152.4"/>
<instance part="L12" gate="G$1" x="20.32" y="101.6"/>
<instance part="L13" gate="G$1" x="40.64" y="101.6"/>
<instance part="L14" gate="G$1" x="60.96" y="101.6"/>
<instance part="L15" gate="G$1" x="81.28" y="101.6"/>
<instance part="L16" gate="G$1" x="101.6" y="101.6"/>
<instance part="SUPPLY1" gate="GND" x="20.32" y="139.7"/>
<instance part="SUPPLY2" gate="GND" x="40.64" y="139.7"/>
<instance part="SUPPLY3" gate="GND" x="60.96" y="139.7"/>
<instance part="SUPPLY4" gate="GND" x="81.28" y="139.7"/>
<instance part="SUPPLY5" gate="GND" x="101.6" y="139.7"/>
<instance part="SUPPLY6" gate="GND" x="121.92" y="139.7"/>
<instance part="SUPPLY7" gate="GND" x="142.24" y="139.7"/>
<instance part="SUPPLY8" gate="GND" x="162.56" y="139.7"/>
<instance part="SUPPLY9" gate="GND" x="101.6" y="86.36"/>
<instance part="SUPPLY10" gate="GND" x="81.28" y="86.36"/>
<instance part="SUPPLY11" gate="GND" x="60.96" y="86.36"/>
<instance part="SUPPLY12" gate="GND" x="40.64" y="86.36"/>
<instance part="SUPPLY13" gate="GND" x="20.32" y="86.36"/>
<instance part="SUPPLY14" gate="GND" x="223.52" y="137.16"/>
<instance part="SUPPLY15" gate="GND" x="203.2" y="137.16"/>
<instance part="SUPPLY16" gate="GND" x="182.88" y="137.16"/>
<instance part="SUPPLY17" gate="G$1" x="20.32" y="175.26"/>
<instance part="SUPPLY18" gate="G$1" x="40.64" y="175.26"/>
<instance part="SUPPLY19" gate="G$1" x="60.96" y="175.26"/>
<instance part="SUPPLY20" gate="G$1" x="81.28" y="175.26"/>
<instance part="SUPPLY21" gate="G$1" x="101.6" y="175.26"/>
<instance part="SUPPLY22" gate="G$1" x="121.92" y="175.26"/>
<instance part="SUPPLY23" gate="G$1" x="142.24" y="175.26"/>
<instance part="SUPPLY24" gate="G$1" x="162.56" y="175.26"/>
<instance part="SUPPLY25" gate="G$1" x="101.6" y="124.46"/>
<instance part="SUPPLY26" gate="G$1" x="81.28" y="124.46"/>
<instance part="SUPPLY27" gate="G$1" x="60.96" y="124.46"/>
<instance part="SUPPLY28" gate="G$1" x="40.64" y="124.46"/>
<instance part="SUPPLY29" gate="G$1" x="20.32" y="124.46"/>
<instance part="SUPPLY30" gate="G$1" x="223.52" y="175.26"/>
<instance part="SUPPLY31" gate="G$1" x="203.2" y="175.26"/>
<instance part="SUPPLY32" gate="G$1" x="182.88" y="175.26"/>
<instance part="C1" gate="G$1" x="12.7" y="167.64"/>
<instance part="C2" gate="G$1" x="33.02" y="167.64"/>
<instance part="C3" gate="G$1" x="53.34" y="167.64"/>
<instance part="C4" gate="G$1" x="73.66" y="167.64"/>
<instance part="C5" gate="G$1" x="93.98" y="167.64"/>
<instance part="C6" gate="G$1" x="114.3" y="167.64"/>
<instance part="C7" gate="G$1" x="134.62" y="167.64"/>
<instance part="C8" gate="G$1" x="154.94" y="167.64"/>
<instance part="C9" gate="G$1" x="175.26" y="167.64"/>
<instance part="C10" gate="G$1" x="195.58" y="167.64"/>
<instance part="C11" gate="G$1" x="215.9" y="167.64"/>
<instance part="C12" gate="G$1" x="12.7" y="116.84"/>
<instance part="C13" gate="G$1" x="33.02" y="116.84"/>
<instance part="C14" gate="G$1" x="53.34" y="116.84"/>
<instance part="C15" gate="G$1" x="73.66" y="116.84"/>
<instance part="C16" gate="G$1" x="93.98" y="116.84"/>
<instance part="JP1" gate="A" x="53.34" y="12.7"/>
<instance part="JP2" gate="A" x="81.28" y="12.7"/>
<instance part="SUPPLY33" gate="GND" x="43.18" y="22.86"/>
<instance part="SUPPLY34" gate="GND" x="71.12" y="22.86"/>
<instance part="SUPPLY35" gate="G$1" x="35.56" y="20.32"/>
<instance part="SUPPLY36" gate="G$1" x="60.96" y="20.32"/>
<instance part="L17" gate="G$1" x="121.92" y="101.6"/>
<instance part="SUPPLY37" gate="GND" x="121.92" y="86.36"/>
<instance part="SUPPLY38" gate="G$1" x="121.92" y="124.46"/>
<instance part="C17" gate="G$1" x="114.3" y="116.84"/>
<instance part="L18" gate="G$1" x="142.24" y="101.6"/>
<instance part="SUPPLY39" gate="GND" x="142.24" y="86.36"/>
<instance part="SUPPLY40" gate="G$1" x="142.24" y="124.46"/>
<instance part="C18" gate="G$1" x="134.62" y="116.84"/>
<instance part="L19" gate="G$1" x="162.56" y="101.6"/>
<instance part="SUPPLY41" gate="GND" x="162.56" y="86.36"/>
<instance part="SUPPLY42" gate="G$1" x="162.56" y="124.46"/>
<instance part="C19" gate="G$1" x="154.94" y="116.84"/>
<instance part="L20" gate="G$1" x="182.88" y="101.6"/>
<instance part="SUPPLY43" gate="GND" x="182.88" y="86.36"/>
<instance part="SUPPLY44" gate="G$1" x="182.88" y="124.46"/>
<instance part="C20" gate="G$1" x="175.26" y="116.84"/>
<instance part="L21" gate="G$1" x="203.2" y="101.6"/>
<instance part="SUPPLY45" gate="GND" x="203.2" y="86.36"/>
<instance part="SUPPLY46" gate="G$1" x="203.2" y="124.46"/>
<instance part="C21" gate="G$1" x="195.58" y="116.84"/>
<instance part="L22" gate="G$1" x="223.52" y="101.6"/>
<instance part="SUPPLY47" gate="GND" x="223.52" y="86.36"/>
<instance part="SUPPLY48" gate="G$1" x="223.52" y="124.46"/>
<instance part="C22" gate="G$1" x="215.9" y="116.84"/>
<instance part="L23" gate="G$1" x="20.32" y="55.88"/>
<instance part="L24" gate="G$1" x="40.64" y="55.88"/>
<instance part="L25" gate="G$1" x="60.96" y="55.88"/>
<instance part="L26" gate="G$1" x="81.28" y="55.88"/>
<instance part="L27" gate="G$1" x="101.6" y="55.88"/>
<instance part="SUPPLY49" gate="GND" x="101.6" y="40.64"/>
<instance part="SUPPLY50" gate="GND" x="81.28" y="40.64"/>
<instance part="SUPPLY51" gate="GND" x="60.96" y="40.64"/>
<instance part="SUPPLY52" gate="GND" x="40.64" y="40.64"/>
<instance part="SUPPLY53" gate="GND" x="20.32" y="40.64"/>
<instance part="SUPPLY54" gate="G$1" x="101.6" y="78.74"/>
<instance part="SUPPLY55" gate="G$1" x="81.28" y="78.74"/>
<instance part="SUPPLY56" gate="G$1" x="60.96" y="78.74"/>
<instance part="SUPPLY57" gate="G$1" x="40.64" y="78.74"/>
<instance part="SUPPLY58" gate="G$1" x="20.32" y="78.74"/>
<instance part="C23" gate="G$1" x="12.7" y="71.12"/>
<instance part="C24" gate="G$1" x="33.02" y="71.12"/>
<instance part="C25" gate="G$1" x="53.34" y="71.12"/>
<instance part="C26" gate="G$1" x="73.66" y="71.12"/>
<instance part="C27" gate="G$1" x="93.98" y="71.12"/>
<instance part="L28" gate="G$1" x="121.92" y="55.88"/>
<instance part="SUPPLY59" gate="GND" x="121.92" y="40.64"/>
<instance part="SUPPLY60" gate="G$1" x="121.92" y="78.74"/>
<instance part="C28" gate="G$1" x="114.3" y="71.12"/>
<instance part="L29" gate="G$1" x="142.24" y="55.88"/>
<instance part="SUPPLY61" gate="GND" x="142.24" y="40.64"/>
<instance part="SUPPLY62" gate="G$1" x="142.24" y="78.74"/>
<instance part="C29" gate="G$1" x="134.62" y="71.12"/>
<instance part="L30" gate="G$1" x="162.56" y="55.88"/>
<instance part="SUPPLY63" gate="GND" x="162.56" y="40.64"/>
<instance part="SUPPLY64" gate="G$1" x="162.56" y="78.74"/>
<instance part="C30" gate="G$1" x="154.94" y="71.12"/>
<instance part="L31" gate="G$1" x="182.88" y="55.88"/>
<instance part="SUPPLY65" gate="GND" x="182.88" y="40.64"/>
<instance part="SUPPLY66" gate="G$1" x="182.88" y="78.74"/>
<instance part="C31" gate="G$1" x="175.26" y="71.12"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$2" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="DOUT"/>
<pinref part="L2" gate="G$1" pin="DIN"/>
<wire x1="25.4" y1="152.4" x2="35.56" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="DOUT"/>
<pinref part="L3" gate="G$1" pin="DIN"/>
<wire x1="45.72" y1="152.4" x2="55.88" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="DOUT"/>
<pinref part="L4" gate="G$1" pin="DIN"/>
<wire x1="66.04" y1="152.4" x2="76.2" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="DOUT"/>
<pinref part="L5" gate="G$1" pin="DIN"/>
<wire x1="86.36" y1="152.4" x2="96.52" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="L5" gate="G$1" pin="DOUT"/>
<pinref part="L6" gate="G$1" pin="DIN"/>
<wire x1="106.68" y1="152.4" x2="116.84" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="L6" gate="G$1" pin="DOUT"/>
<pinref part="L7" gate="G$1" pin="DIN"/>
<wire x1="127" y1="152.4" x2="137.16" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="L7" gate="G$1" pin="DOUT"/>
<pinref part="L8" gate="G$1" pin="DIN"/>
<wire x1="147.32" y1="152.4" x2="157.48" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="L9" gate="G$1" pin="DOUT"/>
<pinref part="L10" gate="G$1" pin="DIN"/>
<wire x1="187.96" y1="152.4" x2="198.12" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="L10" gate="G$1" pin="DOUT"/>
<pinref part="L11" gate="G$1" pin="DIN"/>
<wire x1="208.28" y1="152.4" x2="218.44" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="L11" gate="G$1" pin="DOUT"/>
<pinref part="L12" gate="G$1" pin="DIN"/>
<wire x1="228.6" y1="152.4" x2="238.76" y2="152.4" width="0.1524" layer="91"/>
<wire x1="238.76" y1="152.4" x2="238.76" y2="132.08" width="0.1524" layer="91"/>
<wire x1="238.76" y1="132.08" x2="5.08" y2="132.08" width="0.1524" layer="91"/>
<wire x1="5.08" y1="132.08" x2="5.08" y2="101.6" width="0.1524" layer="91"/>
<wire x1="5.08" y1="101.6" x2="15.24" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="L12" gate="G$1" pin="DOUT"/>
<pinref part="L13" gate="G$1" pin="DIN"/>
<wire x1="25.4" y1="101.6" x2="35.56" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="L13" gate="G$1" pin="DOUT"/>
<pinref part="L14" gate="G$1" pin="DIN"/>
<wire x1="45.72" y1="101.6" x2="55.88" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="L14" gate="G$1" pin="DOUT"/>
<pinref part="L15" gate="G$1" pin="DIN"/>
<wire x1="66.04" y1="101.6" x2="76.2" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="L15" gate="G$1" pin="DOUT"/>
<pinref part="L16" gate="G$1" pin="DIN"/>
<wire x1="86.36" y1="101.6" x2="96.52" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<pinref part="L1" gate="G$1" pin="GND"/>
<wire x1="20.32" y1="142.24" x2="20.32" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="20.32" y1="144.78" x2="20.32" y2="147.32" width="0.1524" layer="91"/>
<wire x1="12.7" y1="162.56" x2="12.7" y2="144.78" width="0.1524" layer="91"/>
<wire x1="12.7" y1="144.78" x2="20.32" y2="144.78" width="0.1524" layer="91"/>
<junction x="20.32" y="144.78"/>
</segment>
<segment>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<pinref part="L2" gate="G$1" pin="GND"/>
<wire x1="40.64" y1="142.24" x2="40.64" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="40.64" y1="144.78" x2="40.64" y2="147.32" width="0.1524" layer="91"/>
<wire x1="33.02" y1="162.56" x2="33.02" y2="144.78" width="0.1524" layer="91"/>
<wire x1="33.02" y1="144.78" x2="40.64" y2="144.78" width="0.1524" layer="91"/>
<junction x="40.64" y="144.78"/>
</segment>
<segment>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<pinref part="L3" gate="G$1" pin="GND"/>
<wire x1="60.96" y1="142.24" x2="60.96" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="60.96" y1="144.78" x2="60.96" y2="147.32" width="0.1524" layer="91"/>
<wire x1="53.34" y1="162.56" x2="53.34" y2="144.78" width="0.1524" layer="91"/>
<wire x1="53.34" y1="144.78" x2="60.96" y2="144.78" width="0.1524" layer="91"/>
<junction x="60.96" y="144.78"/>
</segment>
<segment>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<pinref part="L4" gate="G$1" pin="GND"/>
<wire x1="81.28" y1="142.24" x2="81.28" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="81.28" y1="144.78" x2="81.28" y2="147.32" width="0.1524" layer="91"/>
<wire x1="73.66" y1="162.56" x2="73.66" y2="144.78" width="0.1524" layer="91"/>
<wire x1="73.66" y1="144.78" x2="81.28" y2="144.78" width="0.1524" layer="91"/>
<junction x="81.28" y="144.78"/>
</segment>
<segment>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<pinref part="L5" gate="G$1" pin="GND"/>
<wire x1="101.6" y1="142.24" x2="101.6" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="101.6" y1="144.78" x2="101.6" y2="147.32" width="0.1524" layer="91"/>
<wire x1="93.98" y1="162.56" x2="93.98" y2="144.78" width="0.1524" layer="91"/>
<wire x1="93.98" y1="144.78" x2="101.6" y2="144.78" width="0.1524" layer="91"/>
<junction x="101.6" y="144.78"/>
</segment>
<segment>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<pinref part="L6" gate="G$1" pin="GND"/>
<wire x1="121.92" y1="142.24" x2="121.92" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="121.92" y1="144.78" x2="121.92" y2="147.32" width="0.1524" layer="91"/>
<wire x1="114.3" y1="162.56" x2="114.3" y2="144.78" width="0.1524" layer="91"/>
<wire x1="114.3" y1="144.78" x2="121.92" y2="144.78" width="0.1524" layer="91"/>
<junction x="121.92" y="144.78"/>
</segment>
<segment>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<pinref part="L7" gate="G$1" pin="GND"/>
<wire x1="142.24" y1="142.24" x2="142.24" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="142.24" y1="144.78" x2="142.24" y2="147.32" width="0.1524" layer="91"/>
<wire x1="134.62" y1="162.56" x2="134.62" y2="144.78" width="0.1524" layer="91"/>
<wire x1="134.62" y1="144.78" x2="142.24" y2="144.78" width="0.1524" layer="91"/>
<junction x="142.24" y="144.78"/>
</segment>
<segment>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<pinref part="L8" gate="G$1" pin="GND"/>
<wire x1="162.56" y1="142.24" x2="162.56" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="162.56" y1="144.78" x2="162.56" y2="147.32" width="0.1524" layer="91"/>
<wire x1="154.94" y1="162.56" x2="154.94" y2="144.78" width="0.1524" layer="91"/>
<wire x1="154.94" y1="144.78" x2="162.56" y2="144.78" width="0.1524" layer="91"/>
<junction x="162.56" y="144.78"/>
</segment>
<segment>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<pinref part="L16" gate="G$1" pin="GND"/>
<wire x1="101.6" y1="88.9" x2="101.6" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="101.6" y1="91.44" x2="101.6" y2="96.52" width="0.1524" layer="91"/>
<wire x1="93.98" y1="111.76" x2="93.98" y2="91.44" width="0.1524" layer="91"/>
<wire x1="93.98" y1="91.44" x2="101.6" y2="91.44" width="0.1524" layer="91"/>
<junction x="101.6" y="91.44"/>
</segment>
<segment>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<pinref part="L15" gate="G$1" pin="GND"/>
<wire x1="81.28" y1="88.9" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="81.28" y1="91.44" x2="81.28" y2="96.52" width="0.1524" layer="91"/>
<wire x1="81.28" y1="91.44" x2="73.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="73.66" y1="91.44" x2="73.66" y2="111.76" width="0.1524" layer="91"/>
<junction x="81.28" y="91.44"/>
</segment>
<segment>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<pinref part="L14" gate="G$1" pin="GND"/>
<wire x1="60.96" y1="88.9" x2="60.96" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="60.96" y1="91.44" x2="60.96" y2="96.52" width="0.1524" layer="91"/>
<wire x1="53.34" y1="111.76" x2="53.34" y2="91.44" width="0.1524" layer="91"/>
<wire x1="53.34" y1="91.44" x2="60.96" y2="91.44" width="0.1524" layer="91"/>
<junction x="60.96" y="91.44"/>
</segment>
<segment>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<pinref part="L13" gate="G$1" pin="GND"/>
<wire x1="40.64" y1="88.9" x2="40.64" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="40.64" y1="91.44" x2="40.64" y2="96.52" width="0.1524" layer="91"/>
<wire x1="40.64" y1="91.44" x2="33.02" y2="91.44" width="0.1524" layer="91"/>
<wire x1="33.02" y1="91.44" x2="33.02" y2="111.76" width="0.1524" layer="91"/>
<junction x="40.64" y="91.44"/>
</segment>
<segment>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<pinref part="L12" gate="G$1" pin="GND"/>
<wire x1="20.32" y1="88.9" x2="20.32" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="20.32" y1="91.44" x2="20.32" y2="96.52" width="0.1524" layer="91"/>
<wire x1="20.32" y1="91.44" x2="12.7" y2="91.44" width="0.1524" layer="91"/>
<wire x1="12.7" y1="91.44" x2="12.7" y2="111.76" width="0.1524" layer="91"/>
<junction x="20.32" y="91.44"/>
</segment>
<segment>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<pinref part="L11" gate="G$1" pin="GND"/>
<wire x1="223.52" y1="139.7" x2="223.52" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="223.52" y1="142.24" x2="223.52" y2="147.32" width="0.1524" layer="91"/>
<wire x1="223.52" y1="142.24" x2="215.9" y2="142.24" width="0.1524" layer="91"/>
<wire x1="215.9" y1="142.24" x2="215.9" y2="162.56" width="0.1524" layer="91"/>
<junction x="223.52" y="142.24"/>
</segment>
<segment>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<pinref part="L10" gate="G$1" pin="GND"/>
<wire x1="203.2" y1="139.7" x2="203.2" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="203.2" y1="142.24" x2="203.2" y2="147.32" width="0.1524" layer="91"/>
<wire x1="203.2" y1="142.24" x2="195.58" y2="142.24" width="0.1524" layer="91"/>
<wire x1="195.58" y1="142.24" x2="195.58" y2="162.56" width="0.1524" layer="91"/>
<junction x="203.2" y="142.24"/>
</segment>
<segment>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<pinref part="L9" gate="G$1" pin="GND"/>
<wire x1="182.88" y1="139.7" x2="182.88" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="182.88" y1="142.24" x2="182.88" y2="147.32" width="0.1524" layer="91"/>
<wire x1="182.88" y1="142.24" x2="175.26" y2="142.24" width="0.1524" layer="91"/>
<wire x1="175.26" y1="142.24" x2="175.26" y2="162.56" width="0.1524" layer="91"/>
<junction x="182.88" y="142.24"/>
</segment>
<segment>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
<wire x1="71.12" y1="25.4" x2="71.12" y2="27.94" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="1"/>
<wire x1="78.74" y1="15.24" x2="66.04" y2="15.24" width="0.1524" layer="91"/>
<wire x1="66.04" y1="15.24" x2="66.04" y2="27.94" width="0.1524" layer="91"/>
<wire x1="71.12" y1="27.94" x2="66.04" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
<wire x1="43.18" y1="25.4" x2="43.18" y2="27.94" width="0.1524" layer="91"/>
<wire x1="43.18" y1="27.94" x2="38.1" y2="27.94" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="1"/>
<wire x1="50.8" y1="15.24" x2="38.1" y2="15.24" width="0.1524" layer="91"/>
<wire x1="38.1" y1="15.24" x2="38.1" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
<pinref part="L17" gate="G$1" pin="GND"/>
<wire x1="121.92" y1="88.9" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="121.92" y1="91.44" x2="121.92" y2="96.52" width="0.1524" layer="91"/>
<wire x1="114.3" y1="111.76" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<wire x1="114.3" y1="91.44" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<junction x="121.92" y="91.44"/>
</segment>
<segment>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
<pinref part="L18" gate="G$1" pin="GND"/>
<wire x1="142.24" y1="88.9" x2="142.24" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="142.24" y1="91.44" x2="142.24" y2="96.52" width="0.1524" layer="91"/>
<wire x1="134.62" y1="111.76" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<wire x1="134.62" y1="91.44" x2="142.24" y2="91.44" width="0.1524" layer="91"/>
<junction x="142.24" y="91.44"/>
</segment>
<segment>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
<pinref part="L19" gate="G$1" pin="GND"/>
<wire x1="162.56" y1="88.9" x2="162.56" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="162.56" y1="91.44" x2="162.56" y2="96.52" width="0.1524" layer="91"/>
<wire x1="154.94" y1="111.76" x2="154.94" y2="91.44" width="0.1524" layer="91"/>
<wire x1="154.94" y1="91.44" x2="162.56" y2="91.44" width="0.1524" layer="91"/>
<junction x="162.56" y="91.44"/>
</segment>
<segment>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
<pinref part="L20" gate="G$1" pin="GND"/>
<wire x1="182.88" y1="88.9" x2="182.88" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="182.88" y1="91.44" x2="182.88" y2="96.52" width="0.1524" layer="91"/>
<wire x1="175.26" y1="111.76" x2="175.26" y2="91.44" width="0.1524" layer="91"/>
<wire x1="175.26" y1="91.44" x2="182.88" y2="91.44" width="0.1524" layer="91"/>
<junction x="182.88" y="91.44"/>
</segment>
<segment>
<pinref part="SUPPLY45" gate="GND" pin="GND"/>
<pinref part="L21" gate="G$1" pin="GND"/>
<wire x1="203.2" y1="88.9" x2="203.2" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="203.2" y1="91.44" x2="203.2" y2="96.52" width="0.1524" layer="91"/>
<wire x1="195.58" y1="111.76" x2="195.58" y2="91.44" width="0.1524" layer="91"/>
<wire x1="195.58" y1="91.44" x2="203.2" y2="91.44" width="0.1524" layer="91"/>
<junction x="203.2" y="91.44"/>
</segment>
<segment>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
<pinref part="L22" gate="G$1" pin="GND"/>
<wire x1="223.52" y1="88.9" x2="223.52" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="223.52" y1="91.44" x2="223.52" y2="96.52" width="0.1524" layer="91"/>
<wire x1="215.9" y1="111.76" x2="215.9" y2="91.44" width="0.1524" layer="91"/>
<wire x1="215.9" y1="91.44" x2="223.52" y2="91.44" width="0.1524" layer="91"/>
<junction x="223.52" y="91.44"/>
</segment>
<segment>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
<pinref part="L27" gate="G$1" pin="GND"/>
<wire x1="101.6" y1="43.18" x2="101.6" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="101.6" y1="45.72" x2="101.6" y2="50.8" width="0.1524" layer="91"/>
<wire x1="93.98" y1="66.04" x2="93.98" y2="45.72" width="0.1524" layer="91"/>
<wire x1="93.98" y1="45.72" x2="101.6" y2="45.72" width="0.1524" layer="91"/>
<junction x="101.6" y="45.72"/>
</segment>
<segment>
<pinref part="SUPPLY50" gate="GND" pin="GND"/>
<pinref part="L26" gate="G$1" pin="GND"/>
<wire x1="81.28" y1="43.18" x2="81.28" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="81.28" y1="45.72" x2="81.28" y2="50.8" width="0.1524" layer="91"/>
<wire x1="81.28" y1="45.72" x2="73.66" y2="45.72" width="0.1524" layer="91"/>
<wire x1="73.66" y1="45.72" x2="73.66" y2="66.04" width="0.1524" layer="91"/>
<junction x="81.28" y="45.72"/>
</segment>
<segment>
<pinref part="SUPPLY51" gate="GND" pin="GND"/>
<pinref part="L25" gate="G$1" pin="GND"/>
<wire x1="60.96" y1="43.18" x2="60.96" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="60.96" y1="45.72" x2="60.96" y2="50.8" width="0.1524" layer="91"/>
<wire x1="53.34" y1="66.04" x2="53.34" y2="45.72" width="0.1524" layer="91"/>
<wire x1="53.34" y1="45.72" x2="60.96" y2="45.72" width="0.1524" layer="91"/>
<junction x="60.96" y="45.72"/>
</segment>
<segment>
<pinref part="SUPPLY52" gate="GND" pin="GND"/>
<pinref part="L24" gate="G$1" pin="GND"/>
<wire x1="40.64" y1="43.18" x2="40.64" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="40.64" y1="45.72" x2="40.64" y2="50.8" width="0.1524" layer="91"/>
<wire x1="40.64" y1="45.72" x2="33.02" y2="45.72" width="0.1524" layer="91"/>
<wire x1="33.02" y1="45.72" x2="33.02" y2="66.04" width="0.1524" layer="91"/>
<junction x="40.64" y="45.72"/>
</segment>
<segment>
<pinref part="SUPPLY53" gate="GND" pin="GND"/>
<pinref part="L23" gate="G$1" pin="GND"/>
<wire x1="20.32" y1="43.18" x2="20.32" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="20.32" y1="45.72" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<wire x1="20.32" y1="45.72" x2="12.7" y2="45.72" width="0.1524" layer="91"/>
<wire x1="12.7" y1="45.72" x2="12.7" y2="66.04" width="0.1524" layer="91"/>
<junction x="20.32" y="45.72"/>
</segment>
<segment>
<pinref part="SUPPLY59" gate="GND" pin="GND"/>
<pinref part="L28" gate="G$1" pin="GND"/>
<wire x1="121.92" y1="43.18" x2="121.92" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="121.92" y1="45.72" x2="121.92" y2="50.8" width="0.1524" layer="91"/>
<wire x1="114.3" y1="66.04" x2="114.3" y2="45.72" width="0.1524" layer="91"/>
<wire x1="114.3" y1="45.72" x2="121.92" y2="45.72" width="0.1524" layer="91"/>
<junction x="121.92" y="45.72"/>
</segment>
<segment>
<pinref part="SUPPLY61" gate="GND" pin="GND"/>
<pinref part="L29" gate="G$1" pin="GND"/>
<wire x1="142.24" y1="43.18" x2="142.24" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="142.24" y1="45.72" x2="142.24" y2="50.8" width="0.1524" layer="91"/>
<wire x1="134.62" y1="66.04" x2="134.62" y2="45.72" width="0.1524" layer="91"/>
<wire x1="134.62" y1="45.72" x2="142.24" y2="45.72" width="0.1524" layer="91"/>
<junction x="142.24" y="45.72"/>
</segment>
<segment>
<pinref part="SUPPLY63" gate="GND" pin="GND"/>
<pinref part="L30" gate="G$1" pin="GND"/>
<wire x1="162.56" y1="43.18" x2="162.56" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="162.56" y1="45.72" x2="162.56" y2="50.8" width="0.1524" layer="91"/>
<wire x1="154.94" y1="66.04" x2="154.94" y2="45.72" width="0.1524" layer="91"/>
<wire x1="154.94" y1="45.72" x2="162.56" y2="45.72" width="0.1524" layer="91"/>
<junction x="162.56" y="45.72"/>
</segment>
<segment>
<pinref part="SUPPLY65" gate="GND" pin="GND"/>
<pinref part="L31" gate="G$1" pin="GND"/>
<wire x1="182.88" y1="43.18" x2="182.88" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="182.88" y1="45.72" x2="182.88" y2="50.8" width="0.1524" layer="91"/>
<wire x1="175.26" y1="66.04" x2="175.26" y2="45.72" width="0.1524" layer="91"/>
<wire x1="175.26" y1="45.72" x2="182.88" y2="45.72" width="0.1524" layer="91"/>
<junction x="182.88" y="45.72"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="SUPPLY17" gate="G$1" pin="5V"/>
<pinref part="L1" gate="G$1" pin="VDD"/>
<wire x1="20.32" y1="175.26" x2="20.32" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="20.32" y1="172.72" x2="20.32" y2="157.48" width="0.1524" layer="91"/>
<wire x1="12.7" y1="170.18" x2="12.7" y2="172.72" width="0.1524" layer="91"/>
<wire x1="12.7" y1="172.72" x2="20.32" y2="172.72" width="0.1524" layer="91"/>
<junction x="20.32" y="172.72"/>
</segment>
<segment>
<pinref part="L2" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY18" gate="G$1" pin="5V"/>
<wire x1="40.64" y1="157.48" x2="40.64" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="40.64" y1="172.72" x2="40.64" y2="175.26" width="0.1524" layer="91"/>
<wire x1="33.02" y1="170.18" x2="33.02" y2="172.72" width="0.1524" layer="91"/>
<wire x1="33.02" y1="172.72" x2="40.64" y2="172.72" width="0.1524" layer="91"/>
<junction x="40.64" y="172.72"/>
</segment>
<segment>
<pinref part="SUPPLY19" gate="G$1" pin="5V"/>
<pinref part="L3" gate="G$1" pin="VDD"/>
<wire x1="60.96" y1="175.26" x2="60.96" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="60.96" y1="172.72" x2="60.96" y2="157.48" width="0.1524" layer="91"/>
<wire x1="53.34" y1="170.18" x2="53.34" y2="172.72" width="0.1524" layer="91"/>
<wire x1="53.34" y1="172.72" x2="60.96" y2="172.72" width="0.1524" layer="91"/>
<junction x="60.96" y="172.72"/>
</segment>
<segment>
<pinref part="SUPPLY20" gate="G$1" pin="5V"/>
<pinref part="L4" gate="G$1" pin="VDD"/>
<wire x1="81.28" y1="175.26" x2="81.28" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="81.28" y1="172.72" x2="81.28" y2="157.48" width="0.1524" layer="91"/>
<wire x1="73.66" y1="170.18" x2="73.66" y2="172.72" width="0.1524" layer="91"/>
<wire x1="73.66" y1="172.72" x2="81.28" y2="172.72" width="0.1524" layer="91"/>
<junction x="81.28" y="172.72"/>
</segment>
<segment>
<pinref part="SUPPLY21" gate="G$1" pin="5V"/>
<pinref part="L5" gate="G$1" pin="VDD"/>
<wire x1="101.6" y1="175.26" x2="101.6" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="101.6" y1="172.72" x2="101.6" y2="157.48" width="0.1524" layer="91"/>
<wire x1="93.98" y1="170.18" x2="93.98" y2="172.72" width="0.1524" layer="91"/>
<wire x1="93.98" y1="172.72" x2="101.6" y2="172.72" width="0.1524" layer="91"/>
<junction x="101.6" y="172.72"/>
</segment>
<segment>
<pinref part="SUPPLY22" gate="G$1" pin="5V"/>
<pinref part="L6" gate="G$1" pin="VDD"/>
<wire x1="121.92" y1="175.26" x2="121.92" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="121.92" y1="172.72" x2="121.92" y2="157.48" width="0.1524" layer="91"/>
<wire x1="114.3" y1="170.18" x2="114.3" y2="172.72" width="0.1524" layer="91"/>
<wire x1="114.3" y1="172.72" x2="121.92" y2="172.72" width="0.1524" layer="91"/>
<junction x="121.92" y="172.72"/>
</segment>
<segment>
<pinref part="SUPPLY23" gate="G$1" pin="5V"/>
<pinref part="L7" gate="G$1" pin="VDD"/>
<wire x1="142.24" y1="175.26" x2="142.24" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="142.24" y1="172.72" x2="142.24" y2="157.48" width="0.1524" layer="91"/>
<wire x1="134.62" y1="170.18" x2="134.62" y2="172.72" width="0.1524" layer="91"/>
<wire x1="134.62" y1="172.72" x2="142.24" y2="172.72" width="0.1524" layer="91"/>
<junction x="142.24" y="172.72"/>
</segment>
<segment>
<pinref part="SUPPLY24" gate="G$1" pin="5V"/>
<pinref part="L8" gate="G$1" pin="VDD"/>
<wire x1="162.56" y1="175.26" x2="162.56" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="162.56" y1="172.72" x2="162.56" y2="157.48" width="0.1524" layer="91"/>
<wire x1="154.94" y1="170.18" x2="154.94" y2="172.72" width="0.1524" layer="91"/>
<wire x1="154.94" y1="172.72" x2="162.56" y2="172.72" width="0.1524" layer="91"/>
<junction x="162.56" y="172.72"/>
</segment>
<segment>
<pinref part="L16" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY25" gate="G$1" pin="5V"/>
<wire x1="101.6" y1="106.68" x2="101.6" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="101.6" y1="121.92" x2="101.6" y2="124.46" width="0.1524" layer="91"/>
<wire x1="93.98" y1="119.38" x2="93.98" y2="121.92" width="0.1524" layer="91"/>
<wire x1="93.98" y1="121.92" x2="101.6" y2="121.92" width="0.1524" layer="91"/>
<junction x="101.6" y="121.92"/>
</segment>
<segment>
<pinref part="L15" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY26" gate="G$1" pin="5V"/>
<wire x1="81.28" y1="106.68" x2="81.28" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="81.28" y1="121.92" x2="81.28" y2="124.46" width="0.1524" layer="91"/>
<wire x1="73.66" y1="119.38" x2="73.66" y2="121.92" width="0.1524" layer="91"/>
<wire x1="73.66" y1="121.92" x2="81.28" y2="121.92" width="0.1524" layer="91"/>
<junction x="81.28" y="121.92"/>
</segment>
<segment>
<pinref part="L14" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY27" gate="G$1" pin="5V"/>
<wire x1="60.96" y1="106.68" x2="60.96" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="60.96" y1="121.92" x2="60.96" y2="124.46" width="0.1524" layer="91"/>
<wire x1="53.34" y1="119.38" x2="53.34" y2="121.92" width="0.1524" layer="91"/>
<wire x1="53.34" y1="121.92" x2="60.96" y2="121.92" width="0.1524" layer="91"/>
<junction x="60.96" y="121.92"/>
</segment>
<segment>
<pinref part="L13" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY28" gate="G$1" pin="5V"/>
<wire x1="40.64" y1="106.68" x2="40.64" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="40.64" y1="121.92" x2="40.64" y2="124.46" width="0.1524" layer="91"/>
<wire x1="33.02" y1="119.38" x2="33.02" y2="121.92" width="0.1524" layer="91"/>
<wire x1="33.02" y1="121.92" x2="40.64" y2="121.92" width="0.1524" layer="91"/>
<junction x="40.64" y="121.92"/>
</segment>
<segment>
<pinref part="L12" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY29" gate="G$1" pin="5V"/>
<wire x1="20.32" y1="106.68" x2="20.32" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="20.32" y1="121.92" x2="20.32" y2="124.46" width="0.1524" layer="91"/>
<wire x1="12.7" y1="119.38" x2="12.7" y2="121.92" width="0.1524" layer="91"/>
<wire x1="12.7" y1="121.92" x2="20.32" y2="121.92" width="0.1524" layer="91"/>
<junction x="20.32" y="121.92"/>
</segment>
<segment>
<pinref part="L11" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY30" gate="G$1" pin="5V"/>
<wire x1="223.52" y1="157.48" x2="223.52" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="223.52" y1="172.72" x2="223.52" y2="175.26" width="0.1524" layer="91"/>
<wire x1="215.9" y1="170.18" x2="215.9" y2="172.72" width="0.1524" layer="91"/>
<wire x1="215.9" y1="172.72" x2="223.52" y2="172.72" width="0.1524" layer="91"/>
<junction x="223.52" y="172.72"/>
</segment>
<segment>
<pinref part="L10" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY31" gate="G$1" pin="5V"/>
<wire x1="203.2" y1="157.48" x2="203.2" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="203.2" y1="172.72" x2="203.2" y2="175.26" width="0.1524" layer="91"/>
<wire x1="195.58" y1="170.18" x2="195.58" y2="172.72" width="0.1524" layer="91"/>
<wire x1="195.58" y1="172.72" x2="203.2" y2="172.72" width="0.1524" layer="91"/>
<junction x="203.2" y="172.72"/>
</segment>
<segment>
<pinref part="L9" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY32" gate="G$1" pin="5V"/>
<wire x1="182.88" y1="157.48" x2="182.88" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="182.88" y1="172.72" x2="182.88" y2="175.26" width="0.1524" layer="91"/>
<wire x1="175.26" y1="170.18" x2="175.26" y2="172.72" width="0.1524" layer="91"/>
<wire x1="175.26" y1="172.72" x2="182.88" y2="172.72" width="0.1524" layer="91"/>
<junction x="182.88" y="172.72"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="2"/>
<wire x1="50.8" y1="12.7" x2="35.56" y2="12.7" width="0.1524" layer="91"/>
<wire x1="35.56" y1="12.7" x2="35.56" y2="20.32" width="0.1524" layer="91"/>
<pinref part="SUPPLY35" gate="G$1" pin="5V"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="2"/>
<wire x1="78.74" y1="12.7" x2="60.96" y2="12.7" width="0.1524" layer="91"/>
<wire x1="60.96" y1="12.7" x2="60.96" y2="20.32" width="0.1524" layer="91"/>
<pinref part="SUPPLY36" gate="G$1" pin="5V"/>
</segment>
<segment>
<pinref part="L17" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY38" gate="G$1" pin="5V"/>
<wire x1="121.92" y1="106.68" x2="121.92" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="121.92" y1="121.92" x2="121.92" y2="124.46" width="0.1524" layer="91"/>
<wire x1="114.3" y1="119.38" x2="114.3" y2="121.92" width="0.1524" layer="91"/>
<wire x1="114.3" y1="121.92" x2="121.92" y2="121.92" width="0.1524" layer="91"/>
<junction x="121.92" y="121.92"/>
</segment>
<segment>
<pinref part="L18" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY40" gate="G$1" pin="5V"/>
<wire x1="142.24" y1="106.68" x2="142.24" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="142.24" y1="121.92" x2="142.24" y2="124.46" width="0.1524" layer="91"/>
<wire x1="134.62" y1="119.38" x2="134.62" y2="121.92" width="0.1524" layer="91"/>
<wire x1="134.62" y1="121.92" x2="142.24" y2="121.92" width="0.1524" layer="91"/>
<junction x="142.24" y="121.92"/>
</segment>
<segment>
<pinref part="L19" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY42" gate="G$1" pin="5V"/>
<wire x1="162.56" y1="106.68" x2="162.56" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="162.56" y1="121.92" x2="162.56" y2="124.46" width="0.1524" layer="91"/>
<wire x1="154.94" y1="119.38" x2="154.94" y2="121.92" width="0.1524" layer="91"/>
<wire x1="154.94" y1="121.92" x2="162.56" y2="121.92" width="0.1524" layer="91"/>
<junction x="162.56" y="121.92"/>
</segment>
<segment>
<pinref part="L20" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY44" gate="G$1" pin="5V"/>
<wire x1="182.88" y1="106.68" x2="182.88" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="182.88" y1="121.92" x2="182.88" y2="124.46" width="0.1524" layer="91"/>
<wire x1="175.26" y1="119.38" x2="175.26" y2="121.92" width="0.1524" layer="91"/>
<wire x1="175.26" y1="121.92" x2="182.88" y2="121.92" width="0.1524" layer="91"/>
<junction x="182.88" y="121.92"/>
</segment>
<segment>
<pinref part="L21" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY46" gate="G$1" pin="5V"/>
<wire x1="203.2" y1="106.68" x2="203.2" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="203.2" y1="121.92" x2="203.2" y2="124.46" width="0.1524" layer="91"/>
<wire x1="195.58" y1="119.38" x2="195.58" y2="121.92" width="0.1524" layer="91"/>
<wire x1="195.58" y1="121.92" x2="203.2" y2="121.92" width="0.1524" layer="91"/>
<junction x="203.2" y="121.92"/>
</segment>
<segment>
<pinref part="L22" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY48" gate="G$1" pin="5V"/>
<wire x1="223.52" y1="106.68" x2="223.52" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="223.52" y1="121.92" x2="223.52" y2="124.46" width="0.1524" layer="91"/>
<wire x1="215.9" y1="119.38" x2="215.9" y2="121.92" width="0.1524" layer="91"/>
<wire x1="215.9" y1="121.92" x2="223.52" y2="121.92" width="0.1524" layer="91"/>
<junction x="223.52" y="121.92"/>
</segment>
<segment>
<pinref part="L27" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY54" gate="G$1" pin="5V"/>
<wire x1="101.6" y1="60.96" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="101.6" y1="76.2" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<wire x1="93.98" y1="73.66" x2="93.98" y2="76.2" width="0.1524" layer="91"/>
<wire x1="93.98" y1="76.2" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
<junction x="101.6" y="76.2"/>
</segment>
<segment>
<pinref part="L26" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY55" gate="G$1" pin="5V"/>
<wire x1="81.28" y1="60.96" x2="81.28" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="81.28" y1="76.2" x2="81.28" y2="78.74" width="0.1524" layer="91"/>
<wire x1="73.66" y1="73.66" x2="73.66" y2="76.2" width="0.1524" layer="91"/>
<wire x1="73.66" y1="76.2" x2="81.28" y2="76.2" width="0.1524" layer="91"/>
<junction x="81.28" y="76.2"/>
</segment>
<segment>
<pinref part="L25" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY56" gate="G$1" pin="5V"/>
<wire x1="60.96" y1="60.96" x2="60.96" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="60.96" y1="76.2" x2="60.96" y2="78.74" width="0.1524" layer="91"/>
<wire x1="53.34" y1="73.66" x2="53.34" y2="76.2" width="0.1524" layer="91"/>
<wire x1="53.34" y1="76.2" x2="60.96" y2="76.2" width="0.1524" layer="91"/>
<junction x="60.96" y="76.2"/>
</segment>
<segment>
<pinref part="L24" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY57" gate="G$1" pin="5V"/>
<wire x1="40.64" y1="60.96" x2="40.64" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="40.64" y1="76.2" x2="40.64" y2="78.74" width="0.1524" layer="91"/>
<wire x1="33.02" y1="73.66" x2="33.02" y2="76.2" width="0.1524" layer="91"/>
<wire x1="33.02" y1="76.2" x2="40.64" y2="76.2" width="0.1524" layer="91"/>
<junction x="40.64" y="76.2"/>
</segment>
<segment>
<pinref part="L23" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY58" gate="G$1" pin="5V"/>
<wire x1="20.32" y1="60.96" x2="20.32" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="20.32" y1="76.2" x2="20.32" y2="78.74" width="0.1524" layer="91"/>
<wire x1="12.7" y1="73.66" x2="12.7" y2="76.2" width="0.1524" layer="91"/>
<wire x1="12.7" y1="76.2" x2="20.32" y2="76.2" width="0.1524" layer="91"/>
<junction x="20.32" y="76.2"/>
</segment>
<segment>
<pinref part="L28" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY60" gate="G$1" pin="5V"/>
<wire x1="121.92" y1="60.96" x2="121.92" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="121.92" y1="76.2" x2="121.92" y2="78.74" width="0.1524" layer="91"/>
<wire x1="114.3" y1="73.66" x2="114.3" y2="76.2" width="0.1524" layer="91"/>
<wire x1="114.3" y1="76.2" x2="121.92" y2="76.2" width="0.1524" layer="91"/>
<junction x="121.92" y="76.2"/>
</segment>
<segment>
<pinref part="L29" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY62" gate="G$1" pin="5V"/>
<wire x1="142.24" y1="60.96" x2="142.24" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="142.24" y1="76.2" x2="142.24" y2="78.74" width="0.1524" layer="91"/>
<wire x1="134.62" y1="73.66" x2="134.62" y2="76.2" width="0.1524" layer="91"/>
<wire x1="134.62" y1="76.2" x2="142.24" y2="76.2" width="0.1524" layer="91"/>
<junction x="142.24" y="76.2"/>
</segment>
<segment>
<pinref part="L30" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY64" gate="G$1" pin="5V"/>
<wire x1="162.56" y1="60.96" x2="162.56" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="162.56" y1="76.2" x2="162.56" y2="78.74" width="0.1524" layer="91"/>
<wire x1="154.94" y1="73.66" x2="154.94" y2="76.2" width="0.1524" layer="91"/>
<wire x1="154.94" y1="76.2" x2="162.56" y2="76.2" width="0.1524" layer="91"/>
<junction x="162.56" y="76.2"/>
</segment>
<segment>
<pinref part="L31" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY66" gate="G$1" pin="5V"/>
<wire x1="182.88" y1="60.96" x2="182.88" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="182.88" y1="76.2" x2="182.88" y2="78.74" width="0.1524" layer="91"/>
<wire x1="175.26" y1="73.66" x2="175.26" y2="76.2" width="0.1524" layer="91"/>
<wire x1="175.26" y1="76.2" x2="182.88" y2="76.2" width="0.1524" layer="91"/>
<junction x="182.88" y="76.2"/>
</segment>
</net>
<net name="IN" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="DIN"/>
<wire x1="15.24" y1="152.4" x2="5.08" y2="152.4" width="0.1524" layer="91"/>
<label x="5.08" y="152.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="3"/>
<wire x1="50.8" y1="10.16" x2="40.64" y2="10.16" width="0.1524" layer="91"/>
<label x="40.64" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="L18" gate="G$1" pin="DIN"/>
<wire x1="127" y1="101.6" x2="137.16" y2="101.6" width="0.1524" layer="91"/>
<pinref part="L17" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="L19" gate="G$1" pin="DIN"/>
<wire x1="147.32" y1="101.6" x2="157.48" y2="101.6" width="0.1524" layer="91"/>
<pinref part="L18" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="OUT3" class="0">
<segment>
<pinref part="L19" gate="G$1" pin="DOUT"/>
<wire x1="167.64" y1="101.6" x2="177.8" y2="101.6" width="0.1524" layer="91"/>
<pinref part="L20" gate="G$1" pin="DIN"/>
</segment>
</net>
<net name="OUT1" class="0">
<segment>
<pinref part="L21" gate="G$1" pin="DOUT"/>
<wire x1="208.28" y1="101.6" x2="218.44" y2="101.6" width="0.1524" layer="91"/>
<pinref part="L22" gate="G$1" pin="DIN"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="L23" gate="G$1" pin="DIN"/>
<wire x1="5.08" y1="55.88" x2="15.24" y2="55.88" width="0.1524" layer="91"/>
<pinref part="L22" gate="G$1" pin="DOUT"/>
<wire x1="228.6" y1="101.6" x2="236.22" y2="101.6" width="0.1524" layer="91"/>
<wire x1="236.22" y1="101.6" x2="236.22" y2="83.82" width="0.1524" layer="91"/>
<wire x1="236.22" y1="83.82" x2="5.08" y2="83.82" width="0.1524" layer="91"/>
<wire x1="5.08" y1="83.82" x2="5.08" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="L23" gate="G$1" pin="DOUT"/>
<pinref part="L24" gate="G$1" pin="DIN"/>
<wire x1="25.4" y1="55.88" x2="35.56" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="L24" gate="G$1" pin="DOUT"/>
<pinref part="L25" gate="G$1" pin="DIN"/>
<wire x1="45.72" y1="55.88" x2="55.88" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="L25" gate="G$1" pin="DOUT"/>
<pinref part="L26" gate="G$1" pin="DIN"/>
<wire x1="66.04" y1="55.88" x2="76.2" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="L26" gate="G$1" pin="DOUT"/>
<pinref part="L27" gate="G$1" pin="DIN"/>
<wire x1="86.36" y1="55.88" x2="96.52" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="L29" gate="G$1" pin="DIN"/>
<wire x1="127" y1="55.88" x2="137.16" y2="55.88" width="0.1524" layer="91"/>
<pinref part="L28" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="L30" gate="G$1" pin="DIN"/>
<wire x1="147.32" y1="55.88" x2="157.48" y2="55.88" width="0.1524" layer="91"/>
<pinref part="L29" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="OUT6" class="0">
<segment>
<pinref part="L30" gate="G$1" pin="DOUT"/>
<wire x1="167.64" y1="55.88" x2="177.8" y2="55.88" width="0.1524" layer="91"/>
<pinref part="L31" gate="G$1" pin="DIN"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="L16" gate="G$1" pin="DOUT"/>
<pinref part="L17" gate="G$1" pin="DIN"/>
<wire x1="106.68" y1="101.6" x2="116.84" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="L20" gate="G$1" pin="DOUT"/>
<pinref part="L21" gate="G$1" pin="DIN"/>
<wire x1="187.96" y1="101.6" x2="198.12" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="L28" gate="G$1" pin="DIN"/>
<pinref part="L27" gate="G$1" pin="DOUT"/>
<wire x1="116.84" y1="55.88" x2="106.68" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="L8" gate="G$1" pin="DOUT"/>
<pinref part="L9" gate="G$1" pin="DIN"/>
<wire x1="167.64" y1="152.4" x2="177.8" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT" class="0">
<segment>
<pinref part="L31" gate="G$1" pin="DOUT"/>
<wire x1="187.96" y1="55.88" x2="210.82" y2="55.88" width="0.1524" layer="91"/>
<label x="200.66" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="3"/>
<wire x1="78.74" y1="10.16" x2="68.58" y2="10.16" width="0.1524" layer="91"/>
<label x="68.58" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="113,1,51.0371,14.1012,JP1,,,,,"/>
<approved hash="113,1,78.9771,14.1012,JP2,,,,,"/>
</errors>
</schematic>
</drawing>
</eagle>
